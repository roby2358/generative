/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

$.Index = function() {
    var THIS = this;
    var canvas = $('#canvas0');

    var cw = 4;
    var h = canvas.height() / cw;
    var w = canvas.width() / cw;
    var grid = null;
    var fillerLength = 8;
//    var filler = [0, 1, 1, 0, 1, 0, 1, 0];
//    var filler = [0, 1, 0, 1, 1, 1, 1, 0];
    var filler = null;

//0 die
//1 survive
//2 grow
//0 0 0 0
//1 0 0 1
//0 1 0 0
//1 1 0 1
//0 0 1 1
//1 0 1 1
//0 1 1 1
//1 1 1 0

    initGrid();
    fill();
    draw();

    function ctx() {
        return canvas.get(0).getContext('2d');
    }

    function initGrid() {
      grid = new Array(h);
      for (var i = 0 ; i < h ; i++) {
        grid[i] = new Array(w);
        for (var j = 0 ; j < w ; j++) {
          grid[i][j] = 0;
        }
      }

      if (! filler) {
        filler = [];
        for (var k = 0 ; k < fillerLength ; k++) {
          filler.push(Math.floor(Math.random() * 2));
        }
        console.log(filler);
      }
    }

    function fill() {
      for (var j = 0 ; j < w ; j++) {
        grid[0][j] = Math.floor(Math.random() * 2);
      }

      for (var i = 1 ; i < h ; i++) {
        for (var j = 0 ; j < w ; j++) {
          var c = grid[i - 1][(j - 1 + w) % w] + grid[i - 1][j] * 2 + grid[i - 1][(j + 1) % w] * 4;
          grid[i][j] = filler[c];
        }
      }
    }

    function draw() {
        var c = ctx();

        // start an empty path to keep the circles from joining (?)
        c.beginPath();

        c.fillStyle = 'rgb(255,255,255)';
        c.fillRect(0, 0, w, h);

        var h = canvas.height() / cw;
        var w = canvas.width() / cw;
        for (var i = 0 ; i < h ; i++) {
          for (var j = 0 ; j < w ; j++) {
            c.fillStyle = 'rgb(' + (grid[i][j] ? '0,0,0' : '255,255,255') + ')';
            c.fillRect(j * cw, i * cw, cw, cw);
          }
        }
    }

    THIS.draw = draw;
}
