/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

var qtest = QUnit.test;

qtest("test transform point identity", function(assert) {
  var p = new $.Point(2,3);
  var p1 = p.transform([geom.identityTransform]);
  assert.equal(p1.x, 2);
  assert.equal(p1.y, 3);
});

qtest("test transform point translate 1", function(assert) {
  var p = new $.Point(2,3);
  var t = [[1,0,.5], [0,1,.5], [0,0,1]];
  var p1 = p.transform([t]);
  assert.equal(p1.x, 2.5);
  assert.equal(p1.y, 3.5);
});

qtest("test transform point translate2", function(assert) {
  var p = new $.Point(2, 4);
  var t = [[1,0,0], [0,1,1], [0,0,1]];
  var p1 = p.transform([t]);
  assert.equal(p1.x, 2);
  assert.equal(p1.y, 5);
});

qtest("test transform point translate1+2+3", function(assert) {
  var p = new $.Point(3,7);
  var t0 = [[1,0,0], [0,1,-1], [0,0,1]];
  var t1 = [[1,0,1], [0,1,0], [0,0,1]];
  var t2 = [[1,0,2], [0,1,-2], [0,0,1]];
  var p1 = p.transform([t0, t1, t2]);
  assert.equal(p1.x, 6);
  assert.equal(p1.y, 4);
});

qtest("test transform point rotate", function(assert) {
  var p = new $.Point(2,2);
  var t = [[.52532,.85090,0], [-.85090,.52532,0], [0,0,1]];
  var p1 = p.transform([t]);
  assertSmallDiff(assert,p1.x,2.75);
  assertSmallDiff(assert,p1.y,-.65);
});