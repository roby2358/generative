var qtest = QUnit.test;

qtest("nested str one", function(assert) {
  var geom = new $.Geometry(0,0);

  function deg(p) {
    console.log(p, geom.segmentAngle(p, [1, 0]) * 180 / math.pi);
  }

  console.log(deg([1, 0]));
  console.log(deg([1.732, 1]));
  console.log(deg([10, 10]));
  console.log(deg([1, 1.732]));
  console.log(deg([0, 1]));
  console.log(deg([-1, 1.732]));
  console.log(deg([-10, 10]));
  console.log(deg([-1.732, 1]));
  console.log(deg([-1, 0]));

  console.log(deg([-1.732, -1]));
  console.log(deg([-10, -10]));
  console.log(deg([-1, -1.732]));
  console.log(deg([0, -1]));
  console.log(deg([1, -1.732]));
  console.log(deg([10, -10]));
  console.log(deg([1.732, -1]));

  assert.equal(1,1);
});

