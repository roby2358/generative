var qtest = QUnit.test;
var phys = new $.Physics(null);

function assertSmallDiff(assert, a, b, message) {
  var result = Math.abs(a - b) < 0.01;

  if (!message) {
    message = result ? "okay" : "failed, expected : small diff [" + a + "] [" + b + "]";
  }

  assert.pushResult({
    result: !!result,
    actual: Math.abs(a - b),
    expected: 0.01,
    message: message
  });
}

qtest( "test spring flat", function(assert) {
  var geom = new $.Geometry(100, 100);
  var phys = new $.Physics(geom);
  var p = new $.Point(1,2);
  var q = new $.Point(3,2);

  var s = new phys.Spring(.1,1);

  var expected = [[1, 0, .1], [0, 1, 0], [0, 0, 1]];
  assert.deepEqual(s.apply(p, q).toArray(), expected);
});

qtest( "test spring slope", function(assert) {
  var geom = new $.Geometry(100, 100);
  var phys = new $.Physics(geom);
  var p = new $.Point(2,3);
  var q = new $.Point(6,0);

  var s = new phys.Spring(.5,1);

  var expected = [[1, 0, 1.6], [0, 1, -1.2], [0, 0, 1]];
  assert.deepEqual(s.apply(p, q).toArray(), expected);
});
