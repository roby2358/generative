/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

var qtest = QUnit.test;

qtest("simple intersect", function(assert) {
  var s1 = new $.Segment(new $.Point(0, 0), new $.Point(2, 2));
  var s2 = new $.Segment(new $.Point(0, 2), new $.Point(2, 0));

  var it0 = s1.intersection(s2);
  assert.equal(it0.x, 1);
  assert.equal(it0.y, 1);

  var it1 = s2.intersection(s1);
  assert.equal(it0.x, 1);
  assert.equal(it0.y, 1);
});

qtest("intersect at end", function(assert) {
  var s1 = new $.Segment(new $.Point(0, 0), new $.Point(2, 2));
  var s2 = new $.Segment(new $.Point(-1, 1), new $.Point(1, -1));

  var it0 = s1.intersection(s2);
  assert.equal(it0.x, 0);
  assert.equal(it0.y, 0);

  var it1 = s2.intersection(s1);
  assert.equal(it0.x, 0);
  assert.equal(it0.y, 0);
});

qtest("no intersect", function(assert) {
  var s1 = new $.Segment(new $.Point(0, 0), new $.Point(2, 2));
  var s2 = new $.Segment(new $.Point(0, 2), new $.Point(2, 4));

  var it0 = s1.intersection(s2);
  assert.equal(it0, null);

  var it1 = s2.intersection(s1);
  assert.equal(it0, null);
});

qtest("overlap", function(assert) {
  var s1 = new $.Segment(new $.Point(0, 0), new $.Point(2, 2));
  var s2 = new $.Segment(new $.Point(1, 1), new $.Point(3, 3));

  var it0 = s1.intersection(s2);
  assert.equal(it0, null);

  var it1 = s2.intersection(s1);
  assert.equal(it0, null);
});
