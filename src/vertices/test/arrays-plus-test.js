var qtest = QUnit.test;

qtest("nested str empty", function(assert) {
  var s = [].nestedStr();
  assert.equal(s, "[]");
});

qtest("nested str one", function(assert) {
  var s = [1].nestedStr();
  assert.equal(s, "[1]");
});

qtest("nested str nested one", function(assert) {
  var s = [[1]].nestedStr();
  assert.equal(s, "[[1]]");
});

qtest("nested str nested many", function(assert) {
  var s = [[1,2],3,[[4]]].nestedStr();
  assert.equal(s, "[[1,2],3,[[4]]]");
});

qtest("create list from square", function(assert) {
  var a = [1, 2, 3];
  var b = a.arrayFrom(function(n) { return n * n; });
  assert.equal(a.nestedStr(), "[1,2,3]");
  assert.equal(b.nestedStr(), "[[1,1],[4,2],[9,3]]");
});

qtest("sort by modulo", function(assert){
  var a = [8,1,162, 487, 745, 982, 369].sortBy(function(n) { return n % 7 });
  // note: sort preserves order for equal values
  assert.equal(a.nestedStr(), "[[1,8],[1,1],[1,162],[2,982],[3,745],[4,487],[5,369]]");
});

qtest("first 2", function(assert) {
  var a = [[1,1],[4,2],[9,3]].firstN(2);
  assert.equal(a.nestedStr(), "[[1,1],[4,2]]");
});

qtest("values of nested arrays", function(assert) {
  var a = [[1,1],[4,2],[9,3]].valuesOf();
  assert.equal(a.nestedStr(), "[1,2,3]");
});

qtest("sum ints", function(assert) {
  var a = [1,2,3].sum();
  assert.equal(a, 6);
});

qtest("sum ints floats", function(assert) {
  var a = [1,2.1,3,4.2].sum();
  assert.equal(a, 10.3);
});

qtest("sum strings", function(assert) {
  var a = ['a', 1, 'c'].sum();
  // hrm
  assert.equal(a, '0a1c');
});