/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

$.Geometry = function(w, h) {
  var THIS = this;
  THIS.w = w;
  THIS.h = h;

  THIS.points = new $.Collection();
  THIS.segments = new $.Collection();

  THIS.randInt = function(n) {
//    return Math.floor((1.0 - Math.random() * Math.random()) * n / 10) * 10;
    var r = 1.0 - Math.sin(Math.random() * 3.141592653589793);
    return Math.floor(r * n / 10) * 10;
  }

  THIS.newRandomPoint = function() {
    var p = new $.Point(THIS.randInt(w), THIS.randInt(h));
    THIS.points.add(p);
    return p;
  }

  THIS.newRandomSidePoint = function(side) {
    var p = null;
    if (side === 0)
      p = new $.Point(0, THIS.randInt(h));
    else if (side === 10)
      p = new $.Point(THIS.randInt(w), 0);
    else if (side === 20)
      p = new $.Point(THIS.randInt(w), h - 1);
    else
      p = new $.Point(w - 1, THIS.randInt(h));
    THIS.points.add(p);
    return p;
  }

  THIS.newSegment = function(p, q) {
    var s = new $.Segment(p, q);
    p.vertices += 1;
    q.vertices += 1;
    THIS.segments.add(s);
    return s;
  }

  /**
   * Identity transform
   */
  this.identityTransform = [[1,0,0], [0,1,0], [0,0,1]];

  /**
   * Reverse a transformation matrix, pointing it in the opposite direction
   *
   * Returns a new matrix, leaving the original alone
   */
  this.reverseTransform = [[-1, 0, 0], [0, -1, 0], [0, 0, 1]];

  // radial force: use arccos(b^2 + c^2 - a^2) / 2bc

  /**
   * Cross product of two vectors in a plane with z=0
   *
   * Also the determinant
   */
  THIS.cross2x2 = function(b, c) {
    return b[0] * c[1] - b[1] * c[0];
  }

  /**
   * Angle at the origin between two points
   */
  THIS.segmentAngle = function(b, c) {
    // math.arccos((b^2 + c^2 - a^2) / 2bc)
    var aa2 = (b[0] - c[0]) * (b[0] - c[0]) + (b[1] - c[1]) * (b[1] - c[1]);
    var bb2 = c[0] * c[0] + c[1] * c[1];
    var cc2 = b[0] * b[0] + b[1] * b[1];
    var co = (bb2 + cc2 - aa2) / (2 * math.sqrt(bb2*cc2));
    var rad = math.acos(co);
    // this is wrong but ok for now
    if (b[1] < 0) {
      rad = 2 * math.pi - rad;
    }
    return rad;
  }

  THIS.centroid = function(ps) {
    var m = ps.reduce(function(a, p) {
      return [a[0] + p.x, a[1] + p.y];
    }, [0, 0]);
    return new $.Point(m[0] / ps.length, m[1] / ps.length);
  }

  THIS.closestN = function(n, p, ps) {
    // for now, just sort and take the top n
    return ps
      .arrayFrom(function(q) { return p.distance(q); })
      .sortByFirst()
      .firstN(n)
      .valuesOf();
  }

  /**
   * Add a vertex count to every point
   */
  THIS.countVertices = function() {
    THIS.points.all.forEach(function(p) {
      p.vertices = 0;
    });

    THIS.segments.all.forEach(function(s) {
      s.p.vertices += 1;
      s.q.vertices += 1;
    });
  }

  THIS.draw = function() {
    background(255);
    fill(0);
    THIS.segments.map(function(s) {
      s.draw();
    });
    THIS.points.map(function(s) {
      s.draw();
    });
  }
}