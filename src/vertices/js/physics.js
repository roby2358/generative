/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

$.Physics = function(geom) {
  var THIS = this;

  /**
   * A spring creates tension on each point proportional to the distance from d
   *
   * A spring creates equal but opposite forces on the endpoints
   */
  this.Spring = function(k, d) {

    this.apply = function(p, q) {
      var segment = new $.Segment(p, q);
      if (segment.length < 10) {
        return math.matrix([[1.0, 0, geom.randInt(3) - 1], [0, 1.0, geom.randInt(3) - 1], [0, 0, 1.0]]);
      }

      var force = k * (segment.length - d);
      var x = force * segment.dx / segment.length;
      var y = force * segment.dy / segment.length;
      // return the force on p, reverse to get force on q
      return math.matrix([[1.0, 0, x], [0, 1.0, y], [0, 0, 1.0]]);
    }
  }

  /**
   * A distance-cubed force function that repels inversely proportional to distance cubed
   */
  this.Repel = function(k) {

    this.apply = function(p, q) {
      var segment = new $.Segment(p, q);
      if (segment.length == 0) {
        return math.matrix([[1.0, 0, 0], [0, 1.0, 0], [0, 0, 1.0]]);
      }

      var force = k * k / (k + segment.length * segment.length * segment.length);
      var x = -1 * force * segment.dx / segment.length;
      var y = -1 * force * segment.dy / segment.length;
      // return the force on p, reverse to get force on q
      return math.matrix([[1.0, 0, x], [0, 1.0, y], [0, 0, 1.0]]);
    }
  }

  /**
   * A distance-cubed force function that repels inversely proportional to distance cubed
   */
  this.Gravity = function(k) {

    this.apply = function(p, q) {
      var segment = new $.Segment(p, q);
      if (segment.length == 0) {
        return math.matrix([[1.0, 0, 0], [0, 1.0, 0], [0, 0, 1.0]]);
      }

      var force = k * k / (k + segment.length * segment.length);
      var x = force * segment.dx / segment.length;
      var y = force * segment.dy / segment.length;
      // return the force on p, reverse to get force on q
      return math.matrix([[1.0, 0, x], [0, 1.0, y], [0, 0, 1.0]]);
    }
  }
}
