/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

$.Index = function() {
  var THIS = this;
  THIS.width = 110 * 5;
  THIS.height = 85 * 5;
  var geom = new $.Geometry(THIS.width, THIS.height);
  var phys = new $.Physics(geom);

//  var num_sides = 7;
//  var num_points = 20;

//  var count = [100];
//    for (var i = 0 ; i < num_points ; i += 1) {
//      geom.newRandomPoint();
//    }
//    // count to 0
//    geom.countVertices();
//    poly(num_sides);

//  function closestToCenter(ps) {
//    var ctr = geom.centroid(ps);
//    return geom.closestN(1, ctr, ps)[0];
//  }
//
//  function poly(n) {
//    var ps = geom.points.all.filter(function(p) { return p.vertices < n / 2; });
//    var r = geom.randInt(ps.length);
//    var ctr = geom.points.all[r];
//    var pp = geom
//      .closestN(n - ctr.vertices + 1, ctr, ps)
//      .slice(1)
//      .sortBy(function(p) {
//        return geom.segmentAngle([p.x - ctr.x, p.y - ctr.y], [100,0]);
//      })
//    var pp = pp.valuesOf();
//    var qq = [];
//    pp.forEach(function(p){
//      if (p !== ctr) {
//        geom.newSegment(ctr,p);
//        if (qq[0]) geom.newSegment(qq[0], p);
//        qq[0] = p;
//      }
//    });
//    geom.newSegment(qq[0], pp[0]);
//  }

  console.log('starting');

  var engine = new $.MeshPull(geom, phys);
  var interval = [null];
  var count = [1000];

  function countdown() {
    count[0] -= 1;
    if (count[0] <= 0) {
      clearInterval(interval[0]);
    }
    engine.doit();
  }

  function go() {
    interval[0] = setInterval(countdown, 10);
  }

  this.draw = geom.draw;
  go();
}
