/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

$.Point = function (x, y) {
  var P = this;
  P.x = x;
  P.y = y;

  var twoPi = Math.PI * 2;
  var pointSize = 5;

  P.reset = function() {
    P.transform = [];
  }
  P.reset();

  P.distance = function(q) {
    var dx = q.x - P.x;
    var dy = q.y - P.y;
    return Math.sqrt(dx * dx + dy * dy);
  }

  function transformOne(a, t) {
    if (!a)
      return t;
    else
      return math.multiply(a, t);
  }

  this.transform = function(ts) {
    if (this.fixed) return P;
    var a = ts.concat([math.matrix([[P.x],[P.y],[1.0]])]).reduce(transformOne, null).toArray();
    P.x = a[0] * a[2];
    P.y = a[1] * a[2];
    return P;
  }

  this.nestedStr = [[x], [y]].nestedStr();

  this.draw = function() {
    circle(this.x, this.y, pointSize);
  }
}
