/**
 * Render the array as a string with nested []
 */
Array.prototype.nestedStr = function () {
  var str = [""];
  this.forEach(function(item) {
    if (str[0] != "") str[0] += ",";
    if (Array.isArray(item)) str[0] += item.nestedStr();
    else str[0] += item.toString();
  });
  return "[" + str[0] + "]";
}

/**
  * make a shallow copy of the array
  */
Array.prototype.copy = function() {
  var t = [];
  for (var i = 0 ; i < this.length ; i += 1) {
    t.push(this[i]);
  }
  return t;
}

/**
 * make a copy with the item removed
 */
Array.prototype.remove = function(that) {
  var t = [];
  for (var i = 0 ; i < this.length ; i += 1) {
    if (this[i] !== that) {
      t.push(this[i]);
    }
  }
  return t;
}

/**
 * Return the sum of the elements in the array
 */
Array.prototype.sum = function() {
  return this.reduce(function(a, c) { return a + c; }, 0);
}

/**
 * Create a list of [f(v), v] from the array
 */
Array.prototype.arrayFrom = function(f) {
  function valuePair(a) {
    return [f(a), a];
  }
  return this.map(valuePair);
}

/**
 * Sort in ascending order by the number in the first array position
 */
Array.prototype.sortByFirst = function() {
  return this.sort(function(a, b) { return a[0] - b[0]; });
}

/**
 * Sort an array of arrays by the first element of the nested arrays
 */
Array.prototype.sortBy = function(f) {
  return this.arrayFrom(f).sortByFirst();
}

/**
 * Take the first n items from the array
 */
Array.prototype.firstN = function(n) {
  return this.slice(0,n);
}

/**
 * Take the second value from an array of nested arrays
 */
Array.prototype.valuesOf = function() {
  return this.map(function(a) { return a[1]; });
}
