/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

$.Segment = function (p, q) {
  var S = this;
  S.p = p;
  S.q = q;

  recalc();

  function recalc() {
    S.dx = q.x - p.x;
    S.dy = q.y - p.y;
    S.length = Math.sqrt(S.dx * S.dx + S.dy * S.dy);
  }

  /**
   * Return the Point that represents the intersection, or null if no point or no one point
   */
  this.intersection = function(that) {
    var d = (p.x - q.x) * (that.p.y - that.q.y) - (p.y - q.y) * (that.p.x - that.q.x);
//    console.log("d", d);

    // no single intersection
    if (math.abs(d) < 0.0001)
      return null;

    var tt = ((p.x - that.p.x) * (that.p.y - that.q.y) - (p.y - that.p.y) * (that.p.x - that.q.x)) / d;
    var ut = - ((p.x - q.x) * (p.y - that.p.y) - (p.y - q.y) * (p.x - that.p.x)) / d;

    // intersection is off the segment
    if (! (0.01 <= tt && tt <= 0.99) || ! (0.01 <= ut && ut <= 0.99))
      return null;

    var x = p.x + (q.x - p.x) * tt;
    var y = p.y + (q.y - p.y) * tt;

    return new $.Point(x, y);
  }

  function draw() {
    line(p.x, p.y, q.x, q.y);
  }

  this.recalc = recalc;
  this.draw = draw;
}
