/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

$.MeshPull = function(geom, phys) {

//    var spring = new phys.Spring(.4,100);
  var spring = new phys.Spring(.1,0);
//    var repel = new phys.Repel(400);
//    var repel = new phys.Repel(400);
  var repel = new phys.Repel(100);
//    var grav = new phys.Gravity(40);
  var grav = new phys.Gravity(10);

  var numSegments = 10;
  var pointCap = 10000;

  var ss = [];

  function add(ss, s) {
    pointCap -= 1;
    if (pointCap <= 0) return ss;

    var tt = ss;
    for (var i = 0 ; i < ss.length ; i += 1) {
      var p = s.intersection(ss[i]);
      if (p) {
        geom.points.all.push(p);
        tt = ss.remove(ss[i]);
        tt = add(tt, new $.Segment(p, s.p));
        tt = add(tt, new $.Segment(p, s.q));
        tt = add(tt, new $.Segment(p, ss[i].p));
        tt = add(tt, new $.Segment(p, ss[i].q));
        return tt;
      }
    }
    tt.push(s);
    return tt;
  }

  for (var i = 0 ; i < numSegments ; i++ ) {
    var side = geom.randInt(40);
    var p = geom.newRandomSidePoint(side);
    var q = geom.newRandomSidePoint(30 - side);
    p.fixed = true;
    q.fixed = true;
    ss = add(ss, new $.Segment(p, q));
  }

  for (var i = 0 ; i < ss.length ; i++) {
    geom.segments.all.push(ss[i]);
  }

  function springs() {
    geom.segments.all.forEach(function(s) {
      var ss = spring.apply(s.p, s.q);
      s.p.transform([ss]);
      s.q.transform([math.inv(ss)]);
      s.recalc();
    });
  }

  function forces() {
    for (var i = 0 ; i < geom.points.all.length ; i += 1) {
      for (var j = i + 1 ; j < geom.points.all.length ; j += 1) {
        var p = geom.points.all[i];
        var q = geom.points.all[j];
        var rt = repel.apply(p, q);
        var gt = grav.apply(p, q);
        p.transform([rt, gt]);
        q.transform([math.inv(rt), math.inv(gt)]);
      }
    }
  }

  function doit() {
    springs();
//    forces();
  }

  this.doit = doit;
}