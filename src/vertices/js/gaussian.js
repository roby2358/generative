$.Gaussian = function(ctx, w, h) {

  // The gaussian values are closely approximated by every other row in Pascal's triangle
  // so I could compute these
  this.gauss1 = [1, 2, 1];
  this.gauss2 = [1, 4, 6, 4, 1];
  this.gauss3 = [1, 6, 15, 20, 15, 6, 1];
  this.gauss4 = [1, 8, 28, 56, 70, 56, 28, 8, 1];
  this.gauss5 = [1, 10, 45, 120, 210, 252, 210, 120, 45, 10, 1];
  this.gauss6 = [1, 12, 66, 220, 495, 792, 924, 792, 495, 220, 66, 12, 1];
  this.gauss7 = [1, 14, 91, 364, 1001, 2002, 3003, 3432, 3003, 2002, 1001, 364, 91, 14, 1];
}