/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

$.Vectors = function(w, h, n, m) {
    var THIS = this;
    var vectors = Array(n);

    // put random unit vectors into the array
    for (var i = 0 ; i < vectors.length ; i++) {
        vectors[i] = randomVector();
    }

    function randval(n) {
        return Math.floor(Math.random() * w);
    }

    function randomVector() {
        var r = Math.random() * 2 * 3.1415;
        return [randval(w),
            randval(h),
            Math.cos(r),
            Math.sin(r),
            Math.random()];
    }

    function findClosest(x, y) {
        var closest = Array(0);
        for (var i = 0 ; i < vectors.length ; i++) {
            var [xx, yy, dx, dy, c] = vectors[i];
            var d = Math.sqrt((xx - x) * (xx - x) + (yy - y) * (yy - y));

            // I think this is ok
            if (closest.length < m) {
                closest.push([d, xx, yy, dx, dy, c]);
            }
            else {
                // bubble sort for the win
                for (var j = 0 ; j < closest.length ; j++) {
                    if (d < closest[j][0]) {
                        var [d0, xx0, yy0, dx0, dy0, c0] = closest[j];
                        closest[j] = [d, xx, yy, dx, dy, c];
                        [d, xx, yy, dx, dy, c] = [d0, xx0, yy0, dx0, dy0, c0];
                    }
                }
            }
        }
        return closest;
    }

    function noise(x, y) {
        var cs = findClosest(x, y);
        var m = 0;
        var t = 0;
        for (var i = 0 ; i < cs.length ; i++) {
            var [d, xx, yy, dx, dy] = cs[i];
//            var dot = (x - xx) / d * dx + (y - yy) / d * dy;
            var cross = (x - xx) / d * dy - (y - yy) / d * dx;

            // this is wrong ;)
//            m = m + cross; // m + dot * (1.0 / d);
//            t = t + Math.abs(cross); // t + (1.0 / d);
            m = m + Math.abs(cross) * (1.0 / d * d);
            t = t + (1.0 / d * d);
        }
        // weighted average by inverse distance
        return m / t;
        // logistic
        // return 1.0 / (1.0 + Math.exp(-m));
    }

    THIS.vectors = vectors;
    THIS.findClosest = findClosest;
    THIS.noise = noise;
}
