/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

$.Index = function() {
    var THIS = this;
    var canvas = $('#canvas0');
    var w = canvas.width();
    var h = canvas.height();
    var cw = 2;
    var hacw = cw / 2;

    var vs = new $.Vectors(w, h, w * h / 400, 3);

    $('#goButton').click(crossprod);
    $('#canvas0').click(noise);

    function showVectors() {
        return $('#checkShowVectors').prop('checked');
    }

    function ctx() {
        return canvas.get(0).getContext('2d');
    }

    function crossprod() {
        var c = ctx();

        c.clearRect(0, 0, w, h);

        var mn = 1000;
        var mx = -1000;
        for (var y = 0 ; y < w ; y = y + cw) {
            for (var x = 0 ; x < w ; x = x + cw) {
                var nn = vs.noise(x, y);
                if (nn < mn) mn = nn;
                if (nn > mx) mx = nn;
                var n = nn * 255;
                c.fillStyle = 'rgb(' + n + ',' + n + ',' + n + ')';
                c.fillRect(x - hacw, y - hacw, cw, cw);
            }
        }

        console.log(mn, mx);

        if (showVectors()) {
            c.fillStyle = 'rgb(0,0,128)';
            for (var i = 0 ; i < vs.vectors.length ; i++ ) {
                var [x, y, dx, dy] = vs.vectors[i];
                c.fillRect(x - hacw, y - hacw, cw, cw);

                c.beginPath();
                c.moveTo(x, y);
                c.lineTo(x + dx * 20, y + dy * 20);
                c.stroke();
            }
        }

        // start an empty path to keep the circles from joining (?)
        c.beginPath();
    }

    function noise(e) {
        var c = ctx();
        var [x ,y] = [e.offsetX, e.offsetY];

        var cs = vs.findClosest(x, y);
        for (var i = 0 ; i < cs.length ; i++) {
            c.fillStyle = 'rgb(255,0,0)';
            c.fillRect(cs[i][1] - hacw, cs[i][2] - hacw, cw, cw);
        }

        var nn = vs.noise(x, y);
        var n = nn * 255;
        c.fillStyle = 'rgb(0,0,' + n + ')';
        c.fillRect(x - hacw, y - hacw, cw, cw);
        return n;
    }

    THIS.crossprod = crossprod;
}
